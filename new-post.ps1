param(
    [parameter(mandatory)] [string] $name
)

$shortcode = (($name -replace "\s+", "-") -replace "[^\w\-]+", "").ToLower()
mkdir "./content/posts/${shortcode}"
(Get-Content .\post_template.md).Replace('%TITLE%', '"' + $name + '"').Replace('%DATE%', (Get-Date -Format o)) | Set-Content "./content/posts/${shortcode}/index.md"
#code "./content/posts/${name}/index.md"
