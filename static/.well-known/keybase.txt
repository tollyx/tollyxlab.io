==================================================================
https://keybase.io/tollyx
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://tollyx.net
  * I am tollyx (https://keybase.io/tollyx) on keybase.
  * I have a public key ASDylLclb7GlzQK4SceIimd3p73Hno8LcXEG9AnjRDvscQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "012096c2663c80d21567062bd6fc38e5f04731237474958b95f0dea3ff41dad2b1600a",
      "host": "keybase.io",
      "kid": "0120f294b7256fb1a5cd02b849c7888a6777a7bdc79e8f0b717106f409e3443bec710a",
      "uid": "fc006dd25ac0cddbf04976ecfb145219",
      "username": "tollyx"
    },
    "merkle_root": {
      "ctime": 1540819818,
      "hash": "e43742b7d792a597a2f5728bde5ebd3b81b382bdeb46213a72b110c955746710e55873ba17dd69af30bd273311ce8a23b79a6334166c960a6821952610621519",
      "hash_meta": "ee88ba5047f7d478afeb7dc74361536706f419d666faffd9bb42a1129d00f220",
      "seqno": 3864847
    },
    "service": {
      "entropy": "wa27Mrf3hur8gMX/YUgLgHzR",
      "hostname": "tollyx.net",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.8.0"
  },
  "ctime": 1540819874,
  "expire_in": 504576000,
  "prev": "1c8729908483dc6e896d70ed8bfbf246dd2ea8ad161e6021e4e7cfba0e8272d5",
  "seqno": 36,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg8pS3JW+xpc0CuEnHiIpnd6e9x56PC3FxBvQJ40Q77HEKp3BheWxvYWTESpcCJMQgHIcpkISD3G6JbXDti/vyRt0uqK0WHmAh5OfPug6CctXEIBqQGHK8Y/5l3VkTOh0yGY5XDo4YGSlMGJp6NTmEftzDAgHCo3NpZ8RAIhPwCoUfsVsseaBVjkLM6sqiK7OLWgOwbN+C1IEhx57/5gp+HbUNfxLRWr+8tha1hdM05RdtCjIIxMVDchrsD6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIMxZD6x29XmTWE90dR3ZUOLVdOqQyGebFRT3LKUZTKogo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/tollyx

==================================================================