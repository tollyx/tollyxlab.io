+++
date = 2025-01-21T19:49:19+01:00
draft = false
title = 'Status update: 2025-01-21'
+++

Hoo boy I think this is a record? Almost 5 years? Covid has come and gone since last time I wrote anything on here and now once again there's "exciting" things going on across the pond...

I'm mostly just writing this just because I wanted to update some details on the site (primarily the links to elsewhere I'm lurking on the web) and thought as well to put something new up here to show I'm not dead.

I still _want_ to write and put stuff up on here, hell I even got some drafts lying around in my notes that I never ended up finishing. It's just that I almost always end up convincing myself that I don't really have anything too interesting to share, which I realize is usually bullshit since this is my place to put up and share whatever I want. It doesn't even have to be high effort.

I guess I've been poking around on rewriting the whole blog in my own rust-powered blog engine? Might be worth talking about, but I'm not sure if it's something I'm going to finish, and I'd be playing into the makes-a-blog-just-to-talk-about-making-the-blog stereotype...

Alright, a late new year's resolution: write _at least_ a single decent-effort post on here. I'm thinking it's either going to be about godot or about some SDL3_GPU related stuff in C# I've been playing around with.

Might also try to come up with some easy way for me to put up some low-effort stuff, with an easy way for people to ignore it if they don't want it.

But yeah, the state of the world is a fuck, life is rolling on. Hope we enter some boring times soon.
