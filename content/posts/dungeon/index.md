+++
date = 2018-01-10T17:50:00+01:00
draft = false
title = "Yet another (traditional) roguelike written in c++"
aliases = ["/blog/dungeon/", "/blog/dungeon.html"]

[taxonomies]
tags = ["cpp", "roguelike"]
+++

![work-in-progress screenshot](screenshot.png)

My current go-to pet project is [dungeon](https://github.com/tollyx/dungeon), a
 roguelike which is currently pretty incomplete and basic, but I think I've
 gotten far enough to start showing it off a bit more.

It's a roguelike with your usual fantasy-setting (for now, anyway)
 and mechanically it's going to be pretty typical as well. Eventually though,
 I'm planning on implementing some kind of bodypart system where potions, food,
 traps, enemy attacks and so on can transform your character, giving you both
 positive and negative effects.

Also, Lua scripting. I want as much as possible outside the main game engine be
 scriptable in Lua. Which means items, status effects, skills, traps, actors and
 whatever else that might be fun to have easily modifiable. I'm also using Lua
 for basic data loading but that's just because I'm lazy and why the hell not
 when Lua is already implemented.

The main things that are currently missing are:

- Menus
- In-game UI (including a log)
- Items (with lua-scripted effects)
- Loading enemies from data

After those and a bit more content I'll be implementing whatever cool ideas that
 I come up with.

I'm mainly making it for learning purposes (and for fun), trying out new things
 as I go. That lack of planning combined with not actually spending too much
 time on it means that progress is slow. The fact that I find myself rewriting
 things quite often due to the mentioned lack of planning doesn't help much,
 either.

I'll try to write blog posts about whatever in the game that might be
 interesting for others, but it will probably take a while before they pop up as
 the game is currently pretty basic and will stay so for a while longer.

[You can find the source on github](https://github.com/tollyx/dungeon).
