+++
date = 2017-06-25T22:40:00+01:00
draft = false
title = "Hello world, again"
aliases = ["/blog/hello-world/", "/blog/hello-world.html"]

[taxonomies]
tags = ["hugo", "jekyll"]
+++

So I've yet again remade this website. This time I've moved from [jekyll](https://jekyllrb.com/) to [hugo](https://gohugo.io/), because I've had to reinstall ruby far too many times and there's always some issue with it preventing me from just getting it up and running so I can update this site. Hugo is just a binary, no need to install anything to get it to run. It's far less of a hassle.

If you for whatever reason want to see the old posts (which were basically just dev logs required for some of my school courses), you can find them [here.](@/old/_index.md)
