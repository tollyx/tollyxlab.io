+++
date = 2020-05-16T13:43:00+02:00
draft = false
title = 'Status update: 2020-05-16'

[taxonomies]
tags = ["hugo", "zola", "roguelike", "spork"]
+++

Oh boy, it's been a while since I wrote anything on this website, over two years ago!

But I've been thinking of starting to write posts more often, try this proggramming-blogging thing out that seems to have had some sort of resurgance. I don't know if I have anything others will find interesting to read, but I've got a few drafts lying around that I'm gonna try finishing up. We'll see how it goes. Just don't expect me to post things too often - maybe once or twice a month, at most.

And with that, I've moved the website to yet another static website generator, this time I'm using [zola]. Why? It's written in rust. That's pretty much all I got. It's actually very similar to [hugo], which I used previously, but I don't know - zola kinda feels nicer to work with when it comes to templates/themes?

My latest free-time project I've been working on is yet another roguelike - this time, I'm following a tutorial so that I won't get stuck for too long figuring out architecture stuff. It's being written in rust and the tutorial I'm following is [this one][roguelike-tutorial]. It's pretty great - it's using [specs] which I've tried out multiple times but I never really got a good hang of figuring out how to do stuff with an ECS, so learning more about that has been nice. But I did the stupid mistake of not writing down some small errors in the tutorial, so I'll probably go back and try to find them again and open a few PR's for them. I'll try to write a post or two about the roguelike here as well. But for now, [you can find the source code for it over here][roguelike-src].

In other news, I bought myself an apartment. I'm moving at the start of June, so that's pretty exciting.

Also, in case someone actually got my rss feed in their reader - sorry, it broke again when I switched to zola, and from what I've read it'll break again in version 0.11 due to a change I agree with. You'll have to fix the feed link, twice. Again, sorry about that.

[zola]: https://getzola.org
[hugo]: https://gohugo.io
[roguelike-tutorial]: https://bfnightly.bracketproductions.com/rustbook/chapter_0.html
[specs]: https://crates.io/crates/specs
[roguelike-src]: https://gitlab.com/tollyx/roguelike-tutorial-rs
