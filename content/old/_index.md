+++
aliases = ["/game/"]
title = "game15"

[extra]
hidden = true
+++

Old posts that were written as part of a few courses I studied in game design and programming. Kept here for archival's sake.
