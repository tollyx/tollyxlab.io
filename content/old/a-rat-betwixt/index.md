+++
title = "A Rat Betwixt"
date = 2017-04-27T17:15:00+01:00
author = "Adrian Hedqvist"
aliases = ["/game/2017/04/27/a-rat-betwixt.html"]

[taxonomies]
tags = ["csharp", "unity3d", "game15", "ggc2017"]
+++

![Cover art made by Sebastian](B7n2cLi.png)

This post is already 2 weeks late - so let's get going right away.

It's been over a year since the last series of posts and now I'm back to write about the project I'm curently working on: A Rat Betwixt. It's a turn-based local (for now - probably online in the future) multiplayer co-op(tional?) strategy RPG, with some deception and hidden agendas mixed in.

![Server screenshot](GEJOSNQ.png)

The game is played on one large screen - such as a TV or a projector - and each player have their phone, tablet or laptop as their own controller. This is exciting because every player has their own screen which can show info that's secret from other players. The players also plan out their turn simultaneously as other players, incentivizing discussion between players while planning their turn as well as giving any player with a hidden agenda to decieve others.

We're currently 5 out of 8 weeks into development, where the first two were only concepting and prototyping. Which means the product we currently have is 3 weeks worth of work. Probably more though as it also has several things from the prototype and the artists started making models for it in one of their earlier courses.

![Client screenshot](IJpyYjL.png)

As much as I love to complain about it, we're using Unity to make the game as it's what most of us in the team has the most experience with and we're on an 8 week time limit. The things that are currently functional in the game is the mobile app connecting to and communitacting with the server, movement, combat and some basic AI enemies. A turn system with players choosing "initiative cards" is in place and working as well as a bunch of functional placeholder UI stuff.

I'm pretty proud that we've gotten as far as we have, having to deal with networking between android and PC even if Unity unity takes almost all platform-specific stuff away. No matter what you do networking is always going to be a pain in the ass.

The next few posts will focus on more on specific things about the development rather than an overview that this post is. It will of course be pretty much focused on code since that's my kind of thing.

Also, here's some links to the other team member's blogs:
[Karl Malm](https://karlmalmgamedev.wordpress.com/), producer and programmer.
[Sebastian Engstrand](https://sebastianengstrands.wordpress.com/), lead design and artist.
[Sakarias Ståhl](https://rostfriblog.wordpress.com/), lead artist.
[Anders Schultheiss](https://andersschultheiss.wordpress.com/), lead animator and artist.
[Maximilian Bergström](https://hiddenmaxdesign.wordpress.com/), artist.
[Linus Bjernhagen](https://linusbjernhagendevelop.wordpress.com/), (an awesome) programmer.
