+++
title = "Aaaand we're live!"
date = 2016-02-06T21:09:28+01:00
author = "Adrian Hedqvist"
aliases = ["/2016/02/06/aaaand-were-live.html"]
+++

I don't know how many blogs I've started just to take them down again, but this one is going to stay up for atleast quite some time. The reason why is simply because it's part of an self-reflection assignment for school. So here you'll get to see what the hell I'm doing for school, which will be mostly gamedev stuff.

The site is currently hosted by [github pages](https://pages.github.com/) and is built using [jekyll](https://jekyllrb.com/). And I gotta say, I'm happy that I won't have to deal with wordpress or some other huge CMS that simply doesn't do what I tell it to do. Plus, it's waaay easier to create your own custom stuff for static sites than CMS'es. Currently I'm mostly using the default theme that you get with jekyll, but I'm planning on atleast modifying it to make this site a little bit more my own.

But alright, what can you expect from this blog? Atleast one post once a week every thursday. Because that's what my assignment says. I might post more than that if I find something interesting to post about though.
